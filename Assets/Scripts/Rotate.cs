﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    //метод, що повертає obj по осі Y на 180 градусів
    public static void Turn(GameObject obj)
    {
        obj.transform.Rotate(0, 180, 0);
    }

    //метод, що плавно повертає obj по осі Y на 180 градусів
    public void StartDeltaRotate(GameObject obj)
    {
        StartCoroutine(DeltaRotate(obj));
    }

    private IEnumerator DeltaRotate(GameObject obj)
    {
        Vector3 localAxis = new Vector3(0, 180, 0);
        float deltaAngle = 180f;
        float speedRotation = 300f;
        float total = 0f;

        while (true)
        {
            float d = speedRotation * Time.deltaTime;

            total += d;

            if (total > deltaAngle)
            {
                obj.transform.Rotate(localAxis, deltaAngle - (total - d));

                yield break;
            }
            else
            {
                obj.transform.Rotate(localAxis, d);
            }

            yield return null;
        }
    }
}