﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomeMath : MonoBehaviour
{
    //метод, що створює список випадкових і неповторних (в межах списку) індексів 
    public static List<int> CreateRandomUniqueIndexes(int numberOfIndexesInList, int min, int max)
    {
        if (numberOfIndexesInList <= Mathf.Abs(max - min) && min < max)
        {
            List<int> list = new List<int>();

            for (int i = min; i < max; i++)
            {
                list.Add(i);
            }

            while (list.Count > numberOfIndexesInList)
            {
                var number = Random.Range(0, list.Count);
                list.RemoveAt(number);
            }

            return list;
        }
        else
        {
            return null;
        }
    }
}
