﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private Text   _numberOfPairsText;

    private void Start()
    {
        Card.createdPair += PrintNumberOfPairs;
    }

    public void PrintNumberOfPairs(int number)
    {
        if (_numberOfPairsText != null)
        {
            _numberOfPairsText.text = "Зібрано пар: " + number.ToString();
        }
    }
}