﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using UnityEngine;

public class Card : MonoBehaviour, IPointerClickHandler
{
    //список карток які було обернуто
    private static List<GameObject> _turnedCards;
    private static List<GameObject> _duplicateTurnedCards;

    //поле, що вказує чи повернуто картку сорочкою вгору
    private bool _IsCardTurnedShurtUp = false;

    //кількість пар
    private static int _numberOfPair;

    private static Rotate _rotate;

    //для зміни в лічильнику (UI)
    public delegate void CreatedPair(int number);
    public static event CreatedPair createdPair;

    private void Start()
    {
        _rotate = GameObject.Find("WorldController").GetComponent<Rotate>();

        _turnedCards = new List<GameObject>();
        _duplicateTurnedCards = new List<GameObject>();

        _numberOfPair = 0;

        //поверне карту через 5 сек після початку гри
        Invoke("RotateCard", 5);
    }

    //метод, що реалізує реакцію на натискання
    public void OnPointerClick(PointerEventData eventData)
    {
        TurnCardShirtDown();

        //
        if (_turnedCards.Count == 2)
        {
            string firsTextureName = _turnedCards[0].transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sprite.name;
            string secondTextureName = _turnedCards[1].transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().sprite.name;

            for (int i = 0; i < _turnedCards.Count; i++)
            {
                _duplicateTurnedCards.Add(_turnedCards[i]);
            }
            _turnedCards.Clear();

            //якщо картки однакові
            if (firsTextureName == secondTextureName)
            {
                _numberOfPair++;
                createdPair?.Invoke(_numberOfPair);
                Invoke("DestroyCards", 1);
            }
            //якщо картки різні
            else
            {
                Invoke("WrapCards", 1);
            }
        }
    }

    //метод для зміни значення IsCardTurnedShurtUp на протилежне
    public void ChangeIsCardTurnedShurtUp()
    {
        _IsCardTurnedShurtUp = !_IsCardTurnedShurtUp;
    }

    //метод для повороту
    private void RotateCard()
    {
        //Rotate.Turn(gameObject);
        _rotate.StartDeltaRotate(gameObject);
        ChangeIsCardTurnedShurtUp();
    }

    //метод для повороту карти сорочкою вниз (для гравця)
    private void TurnCardShirtDown()
    {
        if (_IsCardTurnedShurtUp && _turnedCards.Count <= 1 && _duplicateTurnedCards.Count == 0)
        {
            RotateCard();
            _turnedCards.Add(gameObject);
        }
    }

    //метод, який обертає 2 картки 
    private void WrapCards()
    {
        for (int i = 0; i < 2; i++)
        {
            //Rotate.Turn(_duplicateTurnedCards[0]);
            _rotate.StartDeltaRotate(_duplicateTurnedCards[0]);
            _duplicateTurnedCards[0].GetComponent<Card>().ChangeIsCardTurnedShurtUp();
            _duplicateTurnedCards.RemoveAt(0);
        }
    }

    //метод, який прибирає 2 картки 
    private void DestroyCards()
    {
        for (int i = 0; i < 2; i++)
        {
            _duplicateTurnedCards[0].SetActive(false);
            _duplicateTurnedCards.RemoveAt(0);
        }

        //перезавантажити сцену, якщо зібрано 3 пари
        if (_numberOfPair == 3 && _duplicateTurnedCards.Count == 0)
        {
            ControlGame.Restart();
        }
    }
}