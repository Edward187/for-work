﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpawner : MonoBehaviour
{
    //префаб картки
    [SerializeField] private GameObject         _cardPrefab;
    //батьківский об'єкт для створених карток
    [SerializeField] private GameObject         _cardsParent;
    //масив позицій в яких розміщатимуться картки
    [SerializeField] private Transform[]        _cardsTransform;
    //масив спрайтів для карток
    [SerializeField] private Sprite[]           _cardsSprites;
    
    //список всіх створених карток
    private List<GameObject>                    _cards;
    //розмір спрайту відносно його початкового розміру
    private const float _sizeOfSpriteRelativeToOriginal = 0.35f;


    private void Awake()
    {
        _cards = new List<GameObject>();

        //створюємо та розставляємо картки по позиціям
        for (int i = 0; i < _cardsTransform.Length; i++)
        {
            GameObject newCard = Instantiate(_cardPrefab);
            _cards.Add(newCard);
            _cards[i].transform.parent = _cardsParent.transform;
            _cards[i].transform.position = _cardsTransform[i].position;
        }

        //вибираємо 3 випадкові індекси для масиву спрайтів та подвоюємо їх (щоб було по 2 спрайти на кожну пару карток) 
        List<int> textureIndexes = SomeMath.CreateRandomUniqueIndexes(_cards.Count / 2, 0, _cardsSprites.Length);
        for (int i = 0; i < _cards.Count/2; i++)
        {
            textureIndexes.Add(textureIndexes[i]);
        }

        //випадковим чином накладаємо спрайти
        for (int i = 0; i < _cards.Count; i++)
        {
            GameObject cardPicture = _cards[i].transform.GetChild(1).gameObject;

            int number = Random.Range(0, textureIndexes.Count);

            cardPicture.GetComponent<SpriteRenderer>().sprite = _cardsSprites[textureIndexes[number]];
            Vector3 newSpriteScale = new Vector3(_sizeOfSpriteRelativeToOriginal, _sizeOfSpriteRelativeToOriginal, _sizeOfSpriteRelativeToOriginal);
            cardPicture.transform.localScale = newSpriteScale;
            textureIndexes.RemoveAt(number);
        }        
    }
}